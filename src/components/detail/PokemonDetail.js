import React, {Component} from 'react';
import memoize from "memoize-one";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import NameAndArt from './NameAndArt.js';
import TrainerDetails from './TrainerDetails.js';
import * as pokemonActions from '../../actions/pokemonActions.js';

import '../../compiled/css/pokemonDetail.css'


class PokemonDetail extends Component {

  constructor(){
    super()
    const Datastore = require('nedb');
    this.db = new Datastore({ filename: 'pokemon.db', autoload: true });
    this.state = {id: null}
  }

  static getDerivedStateFromProps(props, oldState){
    const {id} = props.pokemon || {id: null};
    const {fullyLoaded} = props;
    const oldId = oldState.id;
    const oldLoaded = oldState.fullyLoaded;
    const idMatch = oldId === id;
    const needSave = idMatch && (fullyLoaded != oldLoaded);
    return({id, fullyLoaded, needSave: needSave})
  }

  selectedPokemon = memoize(
    ({id, fullyLoaded}) =>{
      if(!fullyLoaded){
        this.props.pokemonActions.fullyLoadPokemon(id);
      }
      return this.props.pokemon;
    }
  );

  saveMemo = memoize(
    ({pokemon, needSave}) =>{
      if(needSave){
        console.log({pokemon})
        this.db.update({ id: pokemon.id }, { pokemon }, {}, function (err, numReplaced) {
          console.log({numReplaced})
  // numReplaced = 1
  // The doc #3 has been replaced by { _id: 'id3', planet: 'Pluton' }
  // Note that the _id is kept unchanged, and the document has been replaced
  // (the 'system' and inhabited fields are not here anymore)
});
      }
    }
  );

  render() {
    if(!this.props.pokemon){
      return null;
    }
    if(this.state.needSave){
      this.saveMemo({pokemon: this.props.pokemon, needSave: this.state.needSave})
    }
    const pokemon = this.selectedPokemon(this.props.pokemon)
    const contents = this.props.appMode === 'trainer' ? this.trainerDetails : this.pokefanDetails
    return (
      <div id='pokemon-detail' className={this.props.appMode}>
        {contents(pokemon)}
      </div>
    )
  }

  trainerDetails(pokemon){
    return(
      <TrainerDetails/>
    )
  }

  pokefanDetails(pokemon){
    return(
        <NameAndArt pokemon={pokemon}/>
    )
  }
}

function mapStateToProps(state) {
  return {
    pokemon: state.pokemon.byId[state.pokemon.selected] || state.pokemon.byId[state.pokemon.previous],
    fullyLoaded: (state.pokemon.byId[state.pokemon.selected] || {}).fullyLoaded,
    appMode: state.app.mode
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pokemonActions: bindActionCreators(pokemonActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PokemonDetail);
