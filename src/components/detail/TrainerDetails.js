import React, {Component} from 'react';
import {Stats, StatOrder} from '../../constants/Stats';
import {PokemonTypes} from '../../constants/PokemonTypes';
import {connect} from 'react-redux';
import '../../compiled/css/trainerDetails.css'

class TrainerDetail extends Component {

  stats(){
    const statLine = StatOrder.map((key)=>{
      const base = (this.props.stats || {[key]: {base: 0}})[key].base;
      let longBase = base.toString();
      while(longBase.length < 3) { longBase = '0' + longBase }
      return ( <div key={key} className='stat'>
        <div className='stat-label'>{Stats[key].name}</div>
        <div className='value'> {longBase}</div>
        </div>)
    })
    const statLine1 = statLine.slice(0,3)
    const statLine2 = statLine.slice(3)
    return(
      <div className='stats'>
        <div className='stat-row'>{statLine1}</div>
        <div className='stat-row'>{statLine2}</div>
      </div>
    )
  }

  types({types}){
    if(!types){
      return (<div className='types'><div className='type'>Loading</div></div>)
    }
    const typeLines = types.map((key)=>{
      const typeInfo = PokemonTypes[key] || {name: key, color: 'white'};
      const divStyle = {background: typeInfo.color }
      return ( <div key={key} className='type' style={divStyle}>{typeInfo.name}</div>)
    })
    return(
      <div className='types'>
      {typeLines}
      </div>
    )
  }

  description(){
    return(<p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>)
  }

  render() {
    const {pokemon} = this.props;
    if(!pokemon ){
      return null;
    }

    return (
      <div id='trainer-details'>
        <div className='name-and-type'>
          <div className='name'>{pokemon.longId}. {pokemon.capitalizedName}</div>
          {this.types(pokemon)}
        </div>
        {this.stats(pokemon)}
        {this.description()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    pokemon: state.pokemon.byId[state.pokemon.selected] || state.pokemon.byId[state.pokemon.previous],
    stats: (state.pokemon.byId[state.pokemon.selected] || {stats:null}).stats
  };
}

function mapDispatchToProps(dispatch) {
  return {
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TrainerDetail);