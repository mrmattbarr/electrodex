import React, {PureComponent} from 'react';

export default class NameAndArt extends PureComponent {

  description(){
    return(
      <div className='description'>
        <div>
          <span>{this.props.pokemon.capitalizedName} is a pokémon.</span>
          <span> Can you believe that the official pokémon API doesn{"'"}t have any possible way to grab a description for a pokémon?</span>
          <span> You can find the alternate name for a the route where you find a specific type of berries.</span>
          <span> But you can't find out whether or not Bulbasaurs like lying in the sunlight.</span>
        </div>
      </div>
    )
  }

  render() {
    const {pokemon} = this.props;
    if(!pokemon){
      return null;
    }

    return (
      <div id='name-and-art'>
        <div className='name'>{pokemon.id}. <strong>{pokemon.capitalizedName}</strong></div>
        <div className='picture-and-description'>
          <div className='img-holder'><img src={pokemon.fullArt} alt='Full Art' onLoad={()=>{}}/></div>
          {this.description()}
        </div>
      </div>
    )
  }
}