import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as pokemonActions from '../actions/pokemonActions.js';
import {bindActionCreators} from 'redux';
import PokemonDetail from "./detail/PokemonDetail.js";
import '../compiled/css/dexCase.css';


class DexCase extends Component {

  render() {
    return (
      <div id='dex-case'>
        {this.renderCover()}
        <PokemonDetail/>
      </div>
    )
  }

  renderCover(){
    let coverClass = 'cover';
    if(this.props.pokemon.selected){
      coverClass += ' open';
    }
    if(true){
      coverClass += ' ' + this.props.appMode;
    }
    return(
        <div className={coverClass} onClick={()=>{this.props.pokemonActions.selectPokemon(null)}}/>
    )
  }
}

function mapStateToProps(state) {
  return {
    pokemon: state.pokemon,
    appMode: state.app.mode
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pokemonActions: bindActionCreators(pokemonActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DexCase);
