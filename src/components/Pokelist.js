import React, {Component} from 'react'
import '../compiled/css/pokelist.css'
import {connect} from 'react-redux';
import ListEntry from './ListEntry.js'

class Pokelist extends Component {

  render() {
    return (
      <div id='pokelist' className={this.props.appMode}>
        {this.renderCover()}
        {this.props.pokemon.filteredIds.map((id)=>{
            return <ListEntry pokemon={this.props.pokemon.byId[id]} key={id}/>
        })}
      </div>
    )
  }

  renderCover(){
    let coverClass = 'cover';
    if(this.props.pokemon.ids.length > 0){
      return null;
    }
    return(
        <div className={coverClass} />
    )
  }
}

function mapStateToProps(state) {
  return {
    pokemon: state.pokemon,
    appMode: state.app.mode
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pokelist);
