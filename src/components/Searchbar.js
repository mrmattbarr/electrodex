import React, {Component} from 'react'
import '../compiled/css/searchbar.css'
import Pokelist from "./Pokelist.js";
import {connect} from 'react-redux';
import * as pokemonActions from '../actions/pokemonActions.js';
import ModeSwitcher from './ModeSwitcher.js'
import {bindActionCreators} from 'redux';

class Searchbar extends Component {

  render() {
  const image = require('../images/' + this.props.appMode + '-logo.png');
    return (
      <div id='searchbar' className={this.props.appMode}>
        <div className='background-holder'>
            <div className='logo-holder'>
                <img className='logo' src={image} alt='electrodex logo'/>
            </div>
            <ModeSwitcher/>
            <input placeholder='Search' onChange={this.props.pokemonActions.filterPokemon}/>
            <Pokelist/>
        </div>
      </div>
    )
  }
}


function mapStateToProps(state) {
  return {
    appMode: state.app.mode
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pokemonActions: bindActionCreators(pokemonActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Searchbar);