import React, {Component} from 'react'
import {connect} from 'react-redux';
import * as pokemonActions from '../actions/pokemonActions.js';
import {bindActionCreators} from 'redux';

class ListEntry extends Component {

  render() {
    const {id} = this.props.pokemon;
    let entryClass = 'pokentry';
    if(this.props.selectedPokemon === id){
        entryClass += ' selected';
    }

    return (
        <div className={entryClass} onClick={()=>{this.props.pokemonActions.selectPokemon(id)}}>
            {this.renderLeftSide()}
            {this.renderRightSide()}
        </div>
    )
  }

  renderLeftSide(){
    const {longId, capitalizedName} = this.props.pokemon;
    let pokename = capitalizedName;
    if(this.props.appMode === 'trainer') {
      pokename = longId + '. ' + pokename
    }
    return(
      <div className='name'>{pokename}</div>
    )

  }

  renderRightSide(){
    const {sprite, capitalizedName} = this.props.pokemon;
    if(this.props.appMode === 'trainer') { return null }
    return(
      <div className='sprite-holder'>
          <img src={sprite} alt={capitalizedName + ' sprite'}/>
      </div>
    )
  }
}


function mapStateToProps(state) {
  return {
    selectedPokemon: state.pokemon.selected,
    appMode: state.app.mode
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pokemonActions: bindActionCreators(pokemonActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListEntry);
