import React, {Component} from 'react'
import '../compiled/css/modeSwitcher.css'
import {connect} from 'react-redux';
import * as appActions from '../actions/appActions.js';
import {bindActionCreators} from 'redux';

class ModeSwitcher extends Component {

  toggle(){

    this.props.appActions.setAppMode(this.props.nextMode)
  }

  render() {
  const fanicon = require('../images/pikacheeve.png');
  const trainicon = require('../images/elite.png');

    return (
      <div className='mode-switcher-holder'>
        <div id='mode-switcher' className={this.props.appMode}>
          <div className='fake-border' onClick={this.toggle.bind(this)}/>
          <div className='switch-holder' >
            <div className='pokefan'>
              <div>Pokéfan</div>
              <img src={fanicon} alt='pokefan'/>
            </div>
            <div className='switcher-knob' onClick={this.toggle.bind(this)}/>
            <div className='trainer'>
              <img src={trainicon} alt='trainer'/>
              <div>Trainer</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    appMode: state.app.mode,
    nextMode: state.app.mode === 'pokefan' ? 'trainer' : 'pokefan'
  };
}

function mapDispatchToProps(dispatch) {
  return {
    appActions: bindActionCreators(appActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModeSwitcher);
