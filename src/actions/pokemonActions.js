import * as types from './actionTypes';

const urls = {
  BULK_SUMMARY: 'https://pokeapi.co/api/v2/pokemon?limit=1000',
  POKEMON_FETCH: (id)=> 'https://pokeapi.co/api/v2/pokemon/' + id
}

export function receivePokemon(pokemon, save=false) {
  const ids = [];
  const pokemonById = {};
  pokemon.map((x)=>{
    const urlBits = x.url.split('/').filter((x)=>{return x.length > 0})
    const {name} = x;
    const id = urlBits[urlBits.length - 1];
    if(id > 1000){
      return null;
    }
    let longId = id;
    while(longId.length < 3){ longId = '0' + longId }
    const capitalizedName = name[0].toUpperCase() + name.slice(1);
    const sprite = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + id +'.png';
    const fullArt = 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/' + longId +'.png';
    ids.push(id);
    pokemonById[id] = {id, longId, sprite, name, fullArt, capitalizedName, fullyLoaded: false}
    return null;
  })
  return {type: types.RECEIVE_POKEMON, ids, pokemonById, filteredIds: [...ids]};
}

export function fetchPokemon() {
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

  return dispatch => {
    return fetch(urls.BULK_SUMMARY, {
      method: 'GET',
      headers
    })
    .then(response => response.json())
    .then(({results}) => dispatch(receivePokemon(results)));
  };
}


export function filterPokemon({target}) {
  return (dispatch, getState) => {
    const searchValue = target.value.toLowerCase();
    const {ids, byId} = getState().pokemon;
    const filteredIds = ids.filter((id)=>{
      const pkmn = byId[id];
      const nameMatch = pkmn.name.includes(searchValue);
      return nameMatch;
    })
    dispatch({type: types.FILTER_POKEMON, filteredIds});
  }
}

export function receiveLocalPokemon(pokemon){
    const ids = [];
    const pokemonById = {};
    pokemon.map((pkmn)=>{
      const id = parseInt(pkmn.id)
      ids.push(parseInt(id));
      pokemonById[id] = pkmn;
      if(id === 4){
        console.log({pkmn})
      }
      return null;
    });
    ids.sort((a,b)=>{ return a - b})
    return {type: types.RECEIVE_POKEMON, ids, pokemonById, filteredIds: [...ids]};
}

export function receiveFullPkmn(pkmn){
    return (dispatch, getState) => {
        const pokemon = getState().pokemon.byId[pkmn.id];
        pokemon.fullyLoaded = true;
        pokemon.types = pkmn.types.map((x)=>{return x.type.name});
        pokemon.stats = {};
        pkmn.stats.map((x)=>{
          pokemon.stats[x.stat.name] = {base: x.base_stat, effort: x.effort}
          return null;
        })
      dispatch({type: types.UPDATE_POKEMON, pokemon});
    }
}

export function fullyLoadPokemon(id) {
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

  return dispatch => {
    return fetch(urls.POKEMON_FETCH(id), {
      method: 'GET',
      headers
    })
    .then(response => response.json())
    .then((pkmn) => dispatch(receiveFullPkmn(pkmn)));
  };
}


export function selectPokemon(id) {
  return (dispatch) => {
    dispatch({type: types.SELECT_POKEMON, id});
  }
}