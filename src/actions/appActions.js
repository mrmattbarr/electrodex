import * as types from './actionTypes';


export function setAppMode(mode) {
  return (dispatch) => {
    dispatch({type: types.SET_APP_MODE, mode});
  }
}