export default {
  pokemon: 
  {
    ids: [],
    filteredIds: [],
    byId: {},
    selected: null,
    previous: null
  },
  app: 
  {
    mode: 'pokefan'
  }
};