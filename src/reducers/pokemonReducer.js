import initialState from './initialState';
import {FETCH_POKEMON, RECEIVE_POKEMON, SELECT_POKEMON, UPDATE_POKEMON, FILTER_POKEMON} from '../actions/actionTypes';

export default function pokemon(state = initialState.pokemon, action) {
  let newState;
  switch (action.type) {
    case FETCH_POKEMON:
      return action;
    case RECEIVE_POKEMON:
      return {
        ...state,
        ids: action.ids,
        byId: action.pokemonById,
        filteredIds: action.filteredIds
      };
    case SELECT_POKEMON:
      return {
        ...state,
        selected: action.id,
        previous: state.selected
      };
    case FILTER_POKEMON:
      return {
        ...state,
        filteredIds: action.filteredIds
      };
    case UPDATE_POKEMON:
      newState =  {
        ...state,
        byId: {
          ...state.byId,
          [action.pokemon.id]: action.pokemon
        }
      };
      if(state.selectedPokemon && !action.filteredIds.includes(state.selectedPokemon)){
        newState.selectedPokemon = null;
      }
      return newState;
    default:
      return state;
  }
}