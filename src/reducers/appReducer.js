import initialState from './initialState';
import {SET_APP_MODE} from '../actions/actionTypes';

export default function app(state = initialState.app, action) {
  switch (action.type) {
    case SET_APP_MODE:
      return {
        ...state,
        mode: action.mode
      };
    default:
      return state;
  }
}