import {combineReducers} from 'redux';
import pokemon from './pokemonReducer';
import app from './appReducer';

const rootReducer = combineReducers({
  pokemon,
  app
});

export default rootReducer;