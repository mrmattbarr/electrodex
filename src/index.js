import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import App from './App';

const store = configureStore();

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// Register service worker commented out because it needs to be added to the root
//   to work.  and you will need to update `homepage` in package.json with the S3
//   version dir on each deploy to s3
//
// import registerServiceWorker from './registerServiceWorker';
// registerServiceWorker();
