export const Stats = {
    'attack': {
        name: 'Attack',
        abbreviation: 'ATK'
    },
    'defense': {
        name: 'Defense',
        abbreviation: 'DEF'
    },
    'speed': {
        name: 'Speed',
        abbreviation: 'SPD'
    },
    'hp': {
        name: 'Hit Points',
        abbreviation: 'HP '
    },
    'special-attack': {
        name: 'Sp. Attack',
        abbreviation: 'SpA'
    },
    'special-defense': {
        name: 'Sp. Defense',
        abbreviation: 'SpD'
    }
}

export const StatOrder = ['attack', 'defense', 'speed', 'hp', 'special-attack', 'special-defense']