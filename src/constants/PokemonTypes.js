export const PokemonTypes = {
    poison: {
        name: 'Poison',
        color: '#A819D7'
    },
    grass: {
        name: 'Grass',
        color: '#3E970A'
    },
    normal: {
        name: 'Normal',
        color: '#ACA974'
    },
    fire: {
        name: 'Fire',
        color: '#FC0C0A'
    },
    water: {
        name: 'Water',
        color: '#097ABC'
    },
    fairy: {
        name: 'Fairy',
        color: '#FFA0C2'
    },
    flying: {
        name: 'Flying',
        color: '#5EB9B2'
    },
    bug: {
        name: 'Bug',
        color: '#BDDD6E'
    },
    ground: {
        name: 'Ground',
        color: '#BFAC22'
    },
    psychic: {
        name: 'Psychic',
        color: '#F55792'
    },
    fighting: {
        name: 'Fighting',
        color: '#800B11'
    },
    rock: {
        name: 'Rock',
        color: '#776A3F'
    },
    ice: {
        name: 'Ice',
        color: '#66D1E5'
    },
    ghost: {
        name: 'Ghost',
        color: '#472A53'
    },
    dragon: {
        name: 'Dragon',
        color: '#29036A'
    },
    electric: {
        name: 'Electric',
        color: '#FFDE00'
    },
    dark: {
        name: 'Dark',
        color: '#916852'
    },
    steel: {
        name: 'Steel',
        color: '#EDEEED'
    }
}