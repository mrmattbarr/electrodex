import React, {Component} from 'react';
import {connect} from 'react-redux';
import memoize from "memoize-one";
import {bindActionCreators} from 'redux';
import * as pokemonActions from './actions/pokemonActions.js';
import Searchbar from "./components/Searchbar.js";
import DexCase from "./components/DexCase.js";

import './compiled/css/App.css'


class App extends Component {

  constructor(){
    super();
    const Datastore = require('nedb');
    this.db = new Datastore({ filename: 'pokemon.db', autoload: true });
    this.db.ensureIndex({ fieldName: 'id', unique: true });
    this.db.find({id: '4'}, (err, pkmn)=>{
      console.log({fibar: pkmn})
    })
  }

  getPokemon(){
    this.db.find({}, (err, pokemon)=>{
      console.log({pokemon})
      if(pokemon.length === 0){
        this.props.pokemonActions.fetchPokemon();
      }
      else {
        this.props.pokemonActions.receiveLocalPokemon(pokemon);
      }
    })
  }

  savePokemon(){
    const updates = this.props.ids.map((id)=>{
      return this.props.byIds[id]
    })
    this.db.insert(updates)
  }

  handlePokemonLoad = memoize(
    (ids) =>{
    if(ids.length === 0){
      this.getPokemon()
    }  else {
      this.savePokemon()
    }
  })

  render() {
    this.handlePokemonLoad(this.props.ids);
    return (
      <div id='pokedex'>
        <Searchbar/>
        <DexCase/>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    ids: state.pokemon.ids,
    byIds: state.pokemon.byId
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pokemonActions: bindActionCreators(pokemonActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
