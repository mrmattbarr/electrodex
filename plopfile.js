function getReactImportString({ length, reactImport, i }) {
  switch (true) {
    case length >= 3 && i !== 0 && i !== length - 1:
      return ` ${reactImport}`;
    case length >= 2 && i === length - 1:
      return ` ${reactImport} }`;
    case length > 1 && i === 0:
      return `, { ${reactImport}`;
    case length === 1:
      return `, { ${reactImport} }`;
    case length === 0:
      return '';
  };
}

module.exports = function(plop) {
  plop.setHelper('getReactImports', reactImports => {
    return reactImports.map((reactImport, i) => getReactImportString({ length: reactImports.length, reactImport, i }));
  });
  plop.setGenerator('component', {
    description: 'Create a component',
    // User input prompts provided as arguments to the template
    prompts: [
      {
        type: 'input',
        name: 'path',
        message: `What is the path to the folder you want to create the file under? (starts with src)
  For example, if you want to create the file under src/components/fooBar type: components/fooBar.
      Your Path: `,
      },
      {
        // Input from terminal
        type: 'input',
        // Variable name for this input
        name: 'name',
        // Prompt to display on command line
        message: 'What is the Component name?'
      },
      {
        type: 'checkbox',
        name: 'reactImports',
        message: 'Select all that you want to import from React:',
        choices: ['useState', 'useRef', 'useEffect', 'useLayoutEffect' ],
      },
      {
        type: 'list',
        name: 'propTypes',
        message: 'Do you want to add PropTypes?',
        choices: ['Yes', 'No'],
      },
      // {
      //   type: 'list',
      //   name: 'withRouter',
      //   message: 'Do you want to add React Router withRouter?',
      //   choices: ['Yes', 'No'],
      // },
    ],
    actions: function(data) {
      const { reactImports } = data;
      data.options = {};
      data.options.declareAsConst = data.propTypes === 'Yes' || data.withRouter === 'Yes' ? true : false;
      data.options.propTypes = data.propTypes === 'Yes' ? true : false;
      data.options.withRouter = data.withRouter === 'Yes' ? true : false;
      data.options.useState = reactImports.includes('useState') ? true : false;
      data.options.useRef = reactImports.includes('useRef') ? true : false;
      data.options.useEffect = reactImports.includes('useEffect') ? true : false;
      data.options.useLayoutEffect = reactImports.includes('useLayoutEffect') ? true : false;

      let actions = [
        {
          // Add a new file
          type: 'add',
          // Path for the new file
          path: 'src/{{path}}/{{pascalCase name}}.js',
          // Handlebars template used to generate content of new file
          templateFile: 'plop-templates/Component.js.hbs',
        },
      ];
      if (data.propTypes === 'Yes') {
        actions.push({
          type: 'modify',
          path: 'testFiles/{{pascalCase name}}.js',
          pattern: 'export default function {{pascalCase name}}(props) {',
          template: `const {{pascalCase name}} = props => {`,
        });
      }
      return actions;
    },
  });
};
