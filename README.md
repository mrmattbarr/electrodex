# Its a Pokedex!
A tool to help trainers and fans alike appreciate the pokemon they find. Heavily a WIP.

# Technologies Used...
This app was primarily written to stay up to date and learn a few new technologies. They're listed below.

## Electron
This is my first foray into using electron to generate and distrbute an actual executable file instead of a website. 

## react
React as a part of electron behaves pretty identically to react as part of a standalone application. 

## redux
For data management and front-end app state I went back to working with redux instead of react hooks / contexts. I use a state aggregator as well as actions / reducers.

## nedb
To prepare for creating an app that is only partially dependant on web connection, I've written the app to make use of nedb and serialize pokemon after fetching. 

## flex
One of my goals for this projects was to make it scale cleanly to a wide variety of screen sizes.

## less theming
When in trainer vs pokefan mode I use nested styling to serve the main concerns of each type of user.